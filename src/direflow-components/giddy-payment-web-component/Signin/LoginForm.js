
import React, { Component } from "react";
import { addUser } from '../userReducer/actions';
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from "react-toasts";
import { signInUser} from "../userReducer/functions";
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'

const mapDispatchToProps = {
  addUser
};
export type Props = {
  user:Object,
  isAuthenticated :Boolean
}
 class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
        this.emailTapeUp=this.emailTapeUp.bind(this)
        this.passwordTapeUp=this.passwordTapeUp.bind(this)
        this.checkRemember=this.checkRemember.bind(this)

        // this.state = {
        //   email: '',
        //   password: '',
        //     remember: false
        //
        // }
        //default
      this.state = {
        email: 'faten.ghandour@gmail.com',
        password: 'faten123',
        remember: false,
        verifLogin:''

      }
    }

  async submitForm() {
      var user={
        email:this.state.email,
        password:this.state.password,
        products: this.props.addedItems,
        TotalPrice:this.props.total
      }
    var data=await signInUser(user);
    if (data.status == 200) {
      this.setState({verifLogin:''})
      this.props.addUser(data.user)
    }else{
      this.setState({verifLogin:"l'email ou le mot de passe saisi est incorrect"})

    }
  }
    emailTapeUp(event){
        this.setState({email: event.target.value})
    }
    passwordTapeUp(event){
        this.setState({password: event.target.value})

    }
    checkRemember(){
        this.setState({remember: !this.state.remember})

    }

    render() {
      console.log(this.props.user.isAuthenticated )
      if (this.props.user.isAuthenticated ) {
        return <Redirect to={{
           pathname: '/profile'
        }} />
      }
        return (
          <>

            <div className={"auth-inner"}>

                <h3>Sign In</h3>
              {this.state.verifLogin != "" && <div className="alert alert-danger">
                {this.state.verifLogin}
              </div>
              }
                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" placeholder="Enter email" value={this.state.email}
                           onChange={this.emailTapeUp}/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" value={this.state.password}
                    onChange={this.passwordTapeUp}/>
                </div>

                <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1"
                               onChange={this.checkRemember} checked={this.state.remember} />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                </div>

                <button type="submit" className="btn btn-primary btn-block" onClick={this.submitForm}>Submit</button>
                <p className="forgot-password text-right">
                    Forgot <a href="#">password?</a>
                </p>
      < ToastsContainer store = { ToastsStore } position = { ToastsContainerPosition.TOP_CENTER }
      />


      </div>


      </>
        );
    }
}
const mapStateToProps =(state) => {
   const {user}=state;
  return {
    user:user,
  }
}
export default connect(
  mapStateToProps,mapDispatchToProps)(LoginForm);
