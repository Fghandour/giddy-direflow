
import React, { Component } from "react";
import { connect } from 'react-redux';
import { addUser ,verifierUser} from "../userReducer/actions";
import Avatar from 'react-avatar';
import Icon from 'react-eva-icons';
import openSocket from 'socket.io-client';
import ConfirmPayment from "./ConfirmPayment";
const socket = openSocket('http://192.168.56.1:8000');
const mapDispatchToProps = {
  addUser,
  verifierUser
};
export type Props = {
  user:Object
}
class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      payment:false
    }
  }

  componentDidMount() {
    socket.on('connected', valid => this.props.verifierUser(valid));
    socket.on('paymentInfo', payment => this.setState({payment:payment}));

  }

  render() {
    const photoAvatar=this.props.user.user.photoURL? this.props.user.user.photoURL:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQOYb34Y3AeQDMsWveCo4G8kJUBJB7fqt29mw&usqp=CAU'
    console.log("this.state.payment",this.state.payment)
    return (
      <>
         {  this.state.payment &&   <div className="alert alert-success">
              Payment est effectué avec succée
            </div>
            }
            <div>
        {!this.props.user.isValidate ?
           <ConfirmPayment/>
         :
          <div>
            <div className="alert alert-success">
               Utilisateur {this.props.user.user.displayName} est activé payment
            </div>
            <div className="row">
              <div className="col-3">
                <Avatar size="100" src={photoAvatar} round={true}  />

              </div>
              <div className="align-items-center col-9 d-flex justify-content-start">
              <span>
              {
                this.props.user.user.displayName
              }
              </span>
              </div>

            </div>
            <hr/>
            <div className="row">
              <div className="col-6">
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                  <span className="input-group-text">
                      <Icon
                        name="email-outline"
                        size="medium"
                        fill="gray"
                      />
                  </span>
                  </div>
                  <input type="text" className="form-control" placeholder="email" id="usr" name="email" value={this.props.user.user.email} readOnly={true}/>
                </div>
              </div>
              <div className="col-6">
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                <span className="input-group-text">

                  <Icon
                    name="phone-call-outline"
                    size="medium"
                    fill="gray"
                  />
                  </span>
                  </div>
                  <input type="text" className="form-control" placeholder="phoneNumber" id="usr" name="phoneNumber" value={this.props.user.user.phoneNumber} readOnly={true}/>
                </div>

              </div>

            </div>

          </div>

        }
        </div>
        </>
    );
  }
}
const mapStateToProps =(state) => {
  console.log('state',state)

  return {
    user:state.user
  }
}
export default connect(
  mapStateToProps,mapDispatchToProps)(Profile);
