import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { EventContext, Styled } from 'direflow-component';
import styles from './App.css';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';

import {
  createStore,applyMiddleware, compose } from 'redux';

// redux-logger is a middleware that lets you log every state change
import logger from 'redux-logger';
import 'bootstrap/dist/css/bootstrap.css';
// redux-thunk is a middleware that lets you dispatch async actions
import thunk from 'redux-thunk';
import rootReducer from './reducers';
import Home from "./Home";
const middleware = applyMiddleware(thunk, logger);
const store = createStore(rootReducer,middleware);
const App = (props) => {
  const dispatch = useContext(EventContext);

  return (
    <Styled styles={styles} scoped={false}>
      <Provider store={store}>
       <Home/>
      </Provider>

    </Styled>
  );
};

App.defaultProps = {
  componentTitle: 'Giddy Payment Web Component',
  sampleList: [
    'Create with React',
    'Build as Web Component',
    'Use it anywhere!',
  ],
}

App.propTypes = {
  componentTitle: PropTypes.string,
  sampleList: PropTypes.array,
};

export default (App);

