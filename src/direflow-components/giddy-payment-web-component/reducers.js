import { combineReducers, createStore  } from 'redux';
import  userReducer  from './userReducer/reducer';
const rootReducer = combineReducers({
  user: userReducer,
});

export default rootReducer;
