import React, { Component } from "react";
import {ToastsContainer, ToastsStore,ToastsContainerPosition} from 'react-toasts';
import { signUpUser } from '../userReducer/functions';

export default class SignUp extends Component {
    constructor(props) {
        super(props);
        this.submitForm = this.submitForm.bind(this);

        this.state = {
            FirstName: '',
            LastName: '',
            email: '',
            phoneNumber:'',
            Password:'',
            photoUrl:'http://www.ukm.my/portal/wp-content/plugins/all-in-one-seo-pack/images/default-user-image.png'

        }
    }
  readFile(files){
      var self=this;
    if (files && files[0]) {
      var FR= new FileReader();
      FR.onload = function(e) {
        self.setState({photoUrl:e.target.result})
        // document.getElementById("imgupload").src = e.target.result;
        document.getElementById("imgupload").style.width = "80px";

      };
      FR.readAsDataURL(files[0] );
    }
  }
    async submitForm(){
      var newUser={
        email:this.state.email,
        password:this.state.password,
        FirstName:this.state.FirstName,
        LastName:this.state.LastName,
        phoneNumber:this.state.phoneNumber,
        photoUrl:this.state.photoUrl
      }
      var {data}=await signUpUser(newUser);
      if(data.status==200){
        ToastsStore.success("You successfully vreated the user")
      }
    }
    render() {
        return (

            <div className={"auth-inner signup"}>
                <h3>Sign Up</h3>
              <div className="form-group" style={{textAlign: "center"}}>
                <label style={{display: "block"}} htmlFor="avatar">
                  <img
                   src={this.state.photoUrl}
                  style={{borderRadius: "50px",height: "80px"}} id="imgupload"
                />
                </label>
                <div className="col-md-6">
                  <input type="file" className="form-control" name="avatar" id="avatar" style={{display: "none"}} onChange={ (e) => this.readFile(e.target.files) } />
                </div>
              </div>
              <div className="row">
                <div className="col-6">
                <div className="form-group">
                    <label>First name</label>
                    <input type="text" className="form-control" placeholder="First name"
                     value={this.state.FirstName}
               onChange={(event)=> {this.setState({FirstName: event.target.value})}}/>
                </div>
                </div>
                <div className="col-6">
                <div className="form-group">
                    <label>Last name</label>
                    <input type="text" className="form-control" placeholder="Last name" value={this.state.LastName}
               onChange={(event)=> {this.setState({LastName: event.target.value})}}/>
                </div>
                </div>
                <div className="col-6">
                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" className="form-control" placeholder="Enter email" value={this.state.email}
               onChange={(event)=> {this.setState({email: event.target.value})}}/>
                </div>
                </div>
                <div className="col-6">
                <div className="form-group">
                     <label>Phone number</label>
                     <input type="text" className="form-control" placeholder="Phone Number" value={this.state.phoneNumber}
                       onChange={(event)=> {this.setState({phoneNumber: event.target.value})}}/>
                </div>
                </div>
                <div className="col-6">
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" value={this.state.password}
                      onChange={(event)=> {this.setState({password: event.target.value})}}/>
                </div>
                </div>
                </div>

                <button type="submit" className="btn btn-primary btn-block" onClick={this.submitForm}>Sign Up</button>
                <p className="forgot-password text-right">
                    Already registered <a href="#">sign in?</a>
                </p>
              <ToastsContainer store={ToastsStore} position={ToastsContainerPosition.TOP_CENTER}/>

      </div>
        );
    }
}
