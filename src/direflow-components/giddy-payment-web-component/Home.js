import React, { useContext, useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, useLocation } from "react-router-dom";
import Login from "./Signin/LoginPage";
import SignUp from "./Signup/Signup";
import Profile from './Signin/Profile'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux';
import Avatar from 'react-avatar';
import {LogoutUser } from "./userReducer/actions";
import {logoutUserFunc} from './userReducer/functions';
import Icon from 'react-eva-icons';
import 'bootstrap/dist/css/bootstrap.css';



// redux-logger is a middleware that lets you log every state change
const mapDispatchToProps = {
    LogoutUser
};
const Home = (props) => {

    const photoAvatar=props.user.user.photoURL? props.user.user.photoURL:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQOYb34Y3AeQDMsWveCo4G8kJUBJB7fqt29mw&usqp=CAU';
    const profileClass=props.user.isAuthenticated ? 'container':'';

    const [isLogout, setIsLogout] = useState(false);

    const logout= async () =>{
        console.log("logout")
        var data= await logoutUserFunc();
        if(data.status=='200'){
            props.LogoutUser()
            setIsLogout(true)
        }

    }
    if (isLogout) {
        // history.replace('/sign-in')
        // history.push('/sign-in')
        return <Redirect to={{
            pathname: '/sign-in'
        }} />
    }

    return (
                <Router>
                    <div className={"h-100"}>
                        <nav className="navbar navbar-expand-lg navbar-light">
                            <div className="container">
                                <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                                    {
                                        ! props.user.isAuthenticated  ?
                                            <ul className="navbar-nav ml-auto">

                                                <li className="nav-item  ml-3">
                                                    <Link className="nav-link" to={"/sign-up"}>Sign up</Link>
                                                </li>
                                                <li><Link className="nav-link" to={"/shop"} >Shop</Link></li>
                                                <li><Link className="nav-link" to={"/cart"}>
                                                    <Icon
                                                        name="shopping-cart"
                                                        size="xlarge"     // small, medium, large, xlarge
                                                        animation={{
                                                            type: "pulse",  // zoom, pulse, shake, flip
                                                            hover: true,
                                                            infinite: false
                                                        }}
                                                    />
                                                </Link></li>
                                            </ul>
                                            :null}
                                    {props.user.isValidate &&
                                    <div className='d-flex justify-content-end  w-100'>
                                        <ul className="navbar-nav align-items-center">
                                            <li><Link className="nav-link" to={"/shop"} >Shop</Link></li>
                                            <li><Link className="nav-link" to={"/cart"}>
                                                <Icon
                                                    name="shopping-cart"
                                                    size="xlarge"     // small, medium, large, xlarge
                                                    animation={{
                                                        type: "pulse",  // zoom, pulse, shake, flip
                                                        hover: true,
                                                        infinite: false
                                                    }}
                                                />
                                            </Link></li>
                                            <li className="nav-item">
                                                <Link className="nav-link" onClick={() => logout()}>Logout</Link>
                                            </li>
                                            <li className="nav-item">
                                                <Link className="nav-link" to={"/profile"} ><Avatar size="50" src={photoAvatar} round={true}  /></Link>
                                            </li>
                                        </ul>


                                    </div>
                                    }


                                </div>
                            </div>
                        </nav>

                        <div  className ="auth-wrapper">
                            <div className={ `${profileClass}` }>
                                <Switch>
                                    <Route exact path='/Login' component={Login} />
                                    <Route exact path="/sign-in" component={Login} />
                                    <Route path="/sign-up" component={SignUp} />
                                    <Route exact path="/" component={Login}/>
                                    <Route exact path="/profile" component={Profile}/>
                                </Switch>
                            </div>
                        </div>
                    </div>
                </Router>

    );
};

const mapStateToProps = function(state) {
    return {
        user: state.user,
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Home);

