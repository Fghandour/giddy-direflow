import {
  ADD_USER,
  VERIFIE_USER,
  CLEAR_USER
} from './actionTypes';

export function addUser(userInfo:Object) {
  return {
    type: ADD_USER,
    userInfo
  };
}
export function verifierUser(verified:Boolean) {
  console.log("actions verified",verified)
  return {
    type: VERIFIE_USER,
    verified
  };
}
export function LogoutUser() {
  return {
    type: CLEAR_USER

  };
}
