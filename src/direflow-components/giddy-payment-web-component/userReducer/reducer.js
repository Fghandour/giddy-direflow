import {
  ADD_USER,
  VERIFIE_USER,
  CLEAR_USER
} from './actionTypes';
const initialState = {
  user: {},
  isAuthenticated :false,
  isValidate:false
};

function userReducer(state = initialState, action){
  switch (action.type) {
    case ADD_USER:
      return {
        ...state,
        isAuthenticated : true,
        user: action.userInfo
      };
      case CLEAR_USER:
      return {
        ...state,
        isAuthenticated : false,
        user: {},
        isValidate:false
      };
    case VERIFIE_USER:
      var verif=action.verified
      return  { ...state, isValidate:verif};
  }

  return state;
}

export default userReducer;
